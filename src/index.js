const { createLogger, format, transports } = require('winston')
const { combine, timestamp, label, printf } = format
var moment = require('moment')
var exec = require('child_process').exec
var path = require('path')
var rimraf = require('rimraf')
var s3 = require('s3')
var cron = require('node-cron')

const logFormat = printf(info => {
  return `${info.timestamp} ${info.level}: ${info.message}`
})

var params = {
  aws: {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
  },
  s3: {
    bucketName: process.env.S3_BUCKET,
    prefix: process.env.S3_PREFIX
  },
  mongo: {
    user: process.env.MONGO_USER,
    password: process.env.MONGO_PASSWORD,
    dbName: process.env.MONGO_DB,
    authDb: process.env.MONGO_AUTH_DB ? process.env.MONGO_AUTH_DB : 'admin',
    port: process.env.MONGO_PORT ? process.env.MONGO_PORT : 27017,
    host: process.env.MONGO_HOST
  }
}

const logger = createLogger({
  level: 'info',
  format: combine(
    label({ label: 'right meow!' }),
    timestamp(),
    logFormat
  ),
  transports: [
    new transports.Console({
      colorize: true,
      timestamp: true
    })
  ]
})

var client = s3.createClient({
  s3Options: {
    accessKeyId: params.aws.accessKeyId,
    secretAccessKey: params.aws.secretAccessKey
  }
})

var cronString = process.env.CRON_STRING
if (cronString) {
    logger.info(`Cron set at ${cronString}`)
    cron.schedule(cronString, function () {
      doBackup()
    })
} else {
    doBackup()
}


function doBackup () {
  let folder = moment().format('YYYYMMDDHHmmss')
  let dir = path.join(__dirname, 'data', folder)

  logger.info(`Backup dir ${dir}`)

  let cmd = `mongodump -d ${params.mongo.dbName} -u "${params.mongo.user}" -p "${params.mongo.password}" -o "${dir}" \
    --authenticationDatabase ${params.mongo.authDb} --ssl --port ${params.mongo.port} \
    -h "${params.mongo.host}"
    `
  logger.info('Starting database backup')
  logger.info(`Running command: ${cmd}`)
  exec(cmd, function (error, stdout, stderr) {
    if (error) {
      logger.error(error)
    }
    logger.info(`Backup complete - console output follows...`)
    logger.info(stdout)
    logger.info(stderr)

    var s3params = {
      localDir: dir,
      deleteRemoved: true,
      s3Params: {
        Bucket: params.s3.bucketName,
        Prefix: `${params.s3.prefix}/${folder}`
      }
    }
    var uploader = client.uploadDir(s3params)
    uploader.on('error', function (err) {
      logger.error(`Unable to sync: ${err.stack}`)
    })
    uploader.on('progress', function () {
      logger.info(`Progress ${uploader.progressAmount} ${uploader.progressTotal}`)
    })
    uploader.on('end', function () {
      logger.info('Upload complete')
      logger.info(`Deleting local backup folder`)

      rimraf(dir, function () {
        logger.info(`Backup folder deleted. Backup complete`)
      })
    })
  })
}
