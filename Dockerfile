FROM node:10-slim

RUN apk add --no-cache mongodb mongodb-tools

ADD src /usr/src/app

WORKDIR /usr/src/app
ENV NODE_ENV=production
RUN yarn

CMD node index.js